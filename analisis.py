class ReportePorTipoVacuna:

    def __init__(self, info):
        self.info = info
        self.data = info["data"]
        self.miPais = info["pais"]
        self.vacuna_usada = info["vacuna_usada"]
        self.anos = info["anos"]
        self.meses = info["meses"]
        
    def filtro(self):
        ano = self.ano
        mes = self.mes
        miPais = self.miPais
        data = self.data
        vacuna_usada = self.vacuna_usada
        registro = " | " + ano + "-" + mes + "    | " +"--- no se encontraron registros ---"
        for row in data["by_manufacturer"]:
            pais = row['location']
            if(pais == miPais):
                fecha = row['date']
                aaaa = fecha[0:4]
                mm = fecha[5:7]
                dd = fecha[8:10]
                if(aaaa == ano and mm == mes):
                    vacunados = row['total_vaccinations']
                    vacuna_utilizada = row['vaccine']
                    if(vacuna_utilizada == vacuna_usada):
                        registro =" | " + fecha + " | " + vacunados
        return registro

    def reporte(self):
        info = self.info
        miPais = self.miPais
        vacuna_usada = self.vacuna_usada
        self.data = info["data"]
        anos = info["anos"]
        meses = info["meses"]
        for ano in anos:
            print("-"*72)
            print("Pais: " + miPais + " | vacuna: " + vacuna_usada + " | reporte del " + ano)
            print("-"*72)
            self.ano = ano
            for mes in meses:
                self.mes = mes
                print(self.filtro())
