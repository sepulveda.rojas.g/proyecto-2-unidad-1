import json
import analisis as ana
import analisisSegundoDoc as fil

# se ´apertura el archivo de manera eficiente con with
# puesto que es un archivo muy grande
with open("data.json", "r") as csvfile:
    contenido = csvfile.read()

#se recibe por pantalla el pais 
while(True):
    print("-"*48)
    print("Por favor digite el pais del que desea obtener informacion")
    pais = str(input("pais: "))
    ano = str(input("ano: "))
    mes = str(input("mes: "))
    fecha = str(ano+"-"+mes+"-"+"01")
    print("presione 1 para visualizar datos del fabricante.")
    print("presione 2 para visualizar gestion de vacunacion.")
    rta=str(input(":_"))
    
    info = {}
    info["pais"] = pais
    info["vacuna_usada"] = "Pfizer/BioNTech"
    info["anos"] = ["2020", "2021"]
    info["meses"] = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    info["data"] = json.loads(contenido)
    
    filtro = {
        'country': pais,
        'daily_vaccinations': '',
        'daily_vaccinations_per_million': '',
        'daily_vaccinations_raw': '',
        'date': fecha,
        'iso_code': '',
        'people_fully_vaccinated': '',
        'people_fully_vaccinated_per_hundred': '',
        'people_vaccinated': '',
        'people_vaccinated_per_hundred': '',
        'source_name': '',
        'source_website': '',
        'total_vaccinations': '',
        'total_vaccinations_per_hundred': '',
        'vaccines': ''
    }

    if(rta == "1"):
        # chequear informe de los datos del fabricante 
        # se genera un informe de acuerdo a unos parametros
        # que estarán conformando un objeto compatible con json
        # para hacer del código una herramienta escalable.
        objeto = ana.ReportePorTipoVacuna(info)
        objeto.reporte()
        
    elif(rta == "2"):
        # para chequear la gestion se toma como referencia
        # el año y el mes como parametros determinantes de gestion

        objeto2 = fil.DatosGestion(info)
        objeto2.tomarFiltro(filtro)
        objeto2.visualizarRegistro(0)
        objeto2.visualizarTabla()
        #en caso de querer visualizar mas detalles de gestion:
        #gestionVacunacion.visualizarRegistro(0)
    else:
        print("error")
