import alineacion as ali

class DatosGestion:

    def __init__(self, data):
        self.data = data["data"]["country_vaccinations"]
    
    def tomarRegistro(self, i):
        registro = self.data[i]
        self.pais = registro['country']
        self.vacunacion_diaria = registro['daily_vaccinations']
        self.vacunacion_diaria_por_millon = registro['daily_vaccinations_per_million']
        self.vacunacion_diaria_raw = registro['daily_vaccinations_raw']
        self.vacunacion_fecha = registro['date']
        self.iso_code = registro['iso_code']
        self.personas_totalmente_vacunadas = registro['people_fully_vaccinated']
        self.personas_totalmente_vacunadas_por_centenar = registro['people_fully_vaccinated_per_hundred']
        self.personas_vacunadas = registro['people_vaccinated']
        self.personas_vacunadas_por_centenar = registro['people_vaccinated_per_hundred']
        self.nombre_de_búsqueda = registro['source_name']
        self.web_de_búsqueda = registro['source_website']
        self.vacunación_total = registro['total_vaccinations']
        self.vacunación_total_por_centenar = registro['total_vaccinations_per_hundred']
        self.vacunas = registro['vaccines']

    def tomarFiltro(self, miFiltro):
        registro = miFiltro
        self.filPais = registro['country']
        self.filVacunacion_diaria = registro['daily_vaccinations']
        self.filVacunacion_diaria_por_millon = registro['daily_vaccinations_per_million']
        self.filVacunacion_diaria_raw = registro['daily_vaccinations_raw']
        self.filVacunacion_fecha = registro['date']
        self.filIso_code = registro['iso_code']
        self.filPersonas_totalmente_vacunadas = registro['people_fully_vaccinated']
        self.filPersonas_totalmente_vacunadas_por_centenar = registro['people_fully_vaccinated_per_hundred']
        self.filPersonas_vacunadas = registro['people_vaccinated']
        self.filPersonas_vacunadas_por_centenar = registro['people_vaccinated_per_hundred']
        self.filNombre_de_búsqueda = registro['source_name']
        self.filWeb_de_búsqueda = registro['source_website']
        self.filVacunación_total = registro['total_vaccinations']
        self.filVacunación_total_por_centenar = registro['total_vaccinations_per_hundred']
        self.filVacunas = registro['vaccines']

    # metodos de valiacion segun los filtros - inicio
    # los sigiuentes metodos 
    def validaPais(self, i, miPais):
        registro = self.data[i]
        self.pais = registro['country']
        if(miPais == ""):
            return 1
        elif(self.pais == miPais):
            return 1
        else:
            return 2
        
    def validaVacunacion_diaria(self, i, miVacunacion_diaria):
        registro = self.data[i]
        self.vacunacion_diaria = registro['daily_vaccinations']
        if(miVacunacion_diaria == ""):
            return 1
        elif(self.vacunacion_diaria == miVacunacion_diaria):
            return 1
        else:
            return 2

    def validaVacunacion_diaria_por_millon(self, i, miVacunacion_diaria_por_millon):
        registro = self.data[i]
        self.vacunacion_diaria_por_millon = registro['daily_vaccinations_per_million']
        if(miVacunacion_diaria_por_millon == ""):
            return 1
        elif(self.vacunacion_diaria_por_millon == miVacunacion_diaria_por_millon):
            return 1
        else:
            return 2

    def validaVacunacion_diaria_raw(self, i, miVacunacion_diaria_raw):
        registro = self.data[i]
        self.vacunacion_diaria_raw = registro['daily_vaccinations_raw']
        if(miVacunacion_diaria_raw == ""):
            return 1
        elif(self.vacunacion_diaria_raw == miVacunacion_diaria_raw):
            return 1
        else:
            return 2

    def validaVacunacion_fecha(self, i, miVacunacion_fecha):
        registro = self.data[i]
        self.vacunacion_fecha = registro['date']
        if(miVacunacion_fecha == ""):
            return 1
        elif(self.vacunacion_fecha[:-3] == miVacunacion_fecha[:-3]):
            return 1
        else:
            return 2

    def validaIso_code(self, i, miIso_code):
        registro = self.data[i]
        self.iso_code = registro['iso_code']
        if(miIso_code == ""):
            return 1
        elif(self.iso_code == miIso_code):
            return 1
        else:
            return 2  

    def validaPersonas_totalmente_vacunadas(self, i, misPersonas_totalmente_vacunadas):
        registro = self.data[i]
        self.personas_totalmente_vacunadas = registro['people_fully_vaccinated']
        if(misPersonas_totalmente_vacunadas == ""):
            return 1
        elif(self.personas_totalmente_vacunadas == misPersonas_totalmente_vacunadas):
            return 1
        else:
            return 2 

    def validaPersonas_totalmente_vacunadas_por_centenar(self, i, misPersonas_totalmente_vacunadas_por_centenar):
        registro = self.data[i]
        self.personas_totalmente_vacunadas_por_centenar = registro['people_fully_vaccinated_per_hundred']
        if(misPersonas_totalmente_vacunadas_por_centenar == ""):
            return 1
        elif(self.personas_totalmente_vacunadas_por_centenar == misPersonas_totalmente_vacunadas_por_centenar):
            return 1
        else:
            return 2 

    def validaPersonas_vacunadas(self, i, misPersonas_vacunadas):
        registro = self.data[i]
        self.personas_vacunadas = registro['people_vaccinated']
        if(misPersonas_vacunadas == ""):
            return 1
        elif(self.personas_vacunadas == misPersonas_vacunadas):
            return 1
        else:
            return 2 

    def validaPersonas_vacunadas_por_centenar(self, i, misPersonas_vacunadas_por_centenar):
        registro = self.data[i]
        self.personas_vacunadas_por_centenar = registro['people_vaccinated_per_hundred']
        if(misPersonas_vacunadas_por_centenar == ""):
            return 1
        elif(self.personas_vacunadas_por_centenar == misPersonas_vacunadas_por_centenar):
            return 1
        else:
            return 2 

    def validaNombre_de_búsqueda(self, i, miNombre_de_búsqueda):
        registro = self.data[i]
        self.nombre_de_búsqueda = registro['source_name']
        if(miNombre_de_búsqueda == ""):
            return 1
        elif(self.nombre_de_búsqueda == miNombre_de_búsqueda):
            return 1
        else:
            return 2

    def validaWeb_de_búsqueda(self, i, miWeb_de_búsqueda):
        registro = self.data[i]
        self.web_de_búsqueda = registro['source_website']
        if(miWeb_de_búsqueda == ""):
            return 1
        elif(self.web_de_búsqueda == miWeb_de_búsqueda):
            return 1
        else:
            return 2

    def validaVacunación_total(self, i, miVacunación_total):
        registro = self.data[i]
        self.vacunación_total = registro['total_vaccinations']
        if(miVacunación_total == ""):
            return 1
        elif(self.vacunación_total == miVacunación_total):
            return 1
        else:
            return 2

    def validaVacunación_total_por_centenar(self, i, miVacunación_total_por_centenar):
        registro = self.data[i]
        self.vacunación_total_por_centenar = registro['total_vaccinations_per_hundred']
        if(miVacunación_total_por_centenar == ""):
            return 1
        elif(self.vacunación_total_por_centenar == miVacunación_total_por_centenar):
            return 1
        else:
            return 2

    def validaVacunas(self, i, miVacunas):
        registro = self.data[i]
        self.vacunas = registro['vaccines']
        if(miVacunas == ""):
            return 1
        elif(self.vacunas == miVacunas):
            return 1
        else:
            return 2
            
    # metodos de valiacion segun los filtros - fin
    
    def verRegistro(self):
        pais = self.pais
        vacunacion_diaria = self.vacunacion_diaria
        vacunacion_diaria_por_millon = self.vacunacion_diaria_por_millon
        vacunacion_diaria_raw = self.vacunacion_diaria_raw
        vacunacion_fecha = self.vacunacion_fecha
        iso_code = self.iso_code
        personas_totalmente_vacunadas = self.personas_totalmente_vacunadas
        personas_totalmente_vacunadas_por_centenar = self.personas_totalmente_vacunadas_por_centenar
        personas_vacunadas = self.personas_vacunadas
        personas_vacunadas_por_centenar = self.personas_vacunadas_por_centenar
        nombre_de_búsqueda = self.nombre_de_búsqueda
        web_de_búsqueda = self.web_de_búsqueda
        vacunación_total = self.vacunación_total
        vacunación_total_por_centenar = self.vacunación_total_por_centenar
        vacunas = self.vacunas

        print("-"*48)
        print("pais: " + pais)
        print("-"*48)
        print("informacion diaria del dia: " + vacunacion_fecha)
        print("-"*48)
        print("vacunas colocadas")
        print("-"*48)
        lista = vacunas.split(",")
        n=1
        for i in lista:
            print(str(n)+". "+ i)
            n=n+1
        print("-"*48)
        if(vacunacion_diaria != ""):
            print("vacunacion diaria..................." + vacunacion_diaria)
        else:
            print("vacunacion diaria...................No hay datos")
        if(vacunacion_diaria_por_millon != ""):
            print("    ...por millón........" + vacunacion_diaria_por_millon)
        else:
            print("...por millón........No hay datos")
        if(vacunacion_diaria_raw != ""):
            print("vacunación diaria raw..............." + vacunacion_diaria_raw)
        else:
            print("vacunación diaria raw...............No hay datos")
        if(iso_code != ""):
            print("ISO code............................" + iso_code)
        else:
            print("ISO code............................No hay datos")
        print("-"*48)
        print("demografia:")
        print("-"*48)
        if(personas_totalmente_vacunadas != ""):
            print("totalmente vacunados................" + personas_totalmente_vacunadas)
        else:
            print("totalmente vacunados................No hay datos")
        if(personas_totalmente_vacunadas_por_centenar != ""):
            print("     ...por centenar..." + personas_totalmente_vacunadas_por_centenar)
        else:
            print("     ...por centenar...No hay datos")
        print("-"*48)
        if(personas_vacunadas != ""):
            print("vacunados..........................." + personas_vacunadas)
        else:
            print("vacunados...........................No hay datos")
        if(personas_vacunadas_por_centenar != ""):
            print("... por centenar.............." + personas_vacunadas_por_centenar)
        else:
            print("vacunados por centenar..............No hay datos")
        if(vacunación_total != ""):
            print("vacunación total...................." + vacunación_total)
        else:
            print("vacunación total....................No hay datos")
        if(vacunación_total_por_centenar != ""):
            print("vacunación total por centenar......." + vacunación_total_por_centenar)
        else:
            print("vacunación total por centenar.......No hay datos")
        print("-"*48)
        print("origen de datos: " + nombre_de_búsqueda)
        print("web de búsqueda: " + web_de_búsqueda)
        print("-"*48)

    def encabezadoTabla(self):
        miPais = self.pais
        miVacunas = self.vacunas
        miNombre_de_búsqueda = self.nombre_de_búsqueda
        miWeb_de_búsqueda =  self.web_de_búsqueda
        miIso_code = self.iso_code
            
        print("-"*50)
        print(miPais)
        print(miVacunas)
        print(miNombre_de_búsqueda)
        print(miWeb_de_búsqueda)
        print(miIso_code)
        print("| -------------------------------------------------------------------- |")
        print("|            | diaria     | vacunados    | vacunadas    | total        |")
        print("| fecha      | ---------- | ------------ | ------------ | ------------ |")
        print("|            | por_millon | por_centenar | por centenar | por centenar |")
        print("| -------------------------------------------------------------------- |")

    def imprimirRegistro(self):
        miPais = self.pais
        miVacunas = self.vacunas
        miNombre_de_búsqueda = self.nombre_de_búsqueda
        miWeb_de_búsqueda =  self.web_de_búsqueda
        miIso_code = self.iso_code
        miVacunacion_fecha = self.vacunacion_fecha
        miVacunacion_diaria = self.vacunacion_diaria
        miVacunacion_diaria_por_millon = self.vacunacion_diaria_por_millon
        misPersonas_totalmente_vacunadas = self.personas_totalmente_vacunadas
        misPersonas_totalmente_vacunadas_por_centenar = self.personas_totalmente_vacunadas_por_centenar
        misPersonas_vacunadas = self.personas_vacunadas
        misPersonas_vacunadas_por_centenar = self.personas_vacunadas_por_centenar
        miVacunación_total = self.vacunación_total
        miVacunación_total_por_centenar = self.vacunación_total_por_centenar

        cadena = "| "+str(ali.alineacion(str(miVacunacion_fecha), 10))
        cadena += " | "+str(str(ali.alineacion(str(miVacunacion_diaria), 10)))
        cadena += " | "+str(str(ali.alineacion(str(misPersonas_totalmente_vacunadas), 12)))
        cadena += " | "+str(str(ali.alineacion(str(misPersonas_vacunadas), 12)))
        cadena += " | "+str(str(ali.alineacion(str(miVacunación_total), 12)))+" | "
        print(cadena)
        print("|            | ---------- | ------------ | ------------ | ------------ |")
        cadena = "|           "
        cadena += " | "+str(ali.alineacion(str(miVacunacion_diaria_por_millon), 10))
        cadena += " | "+str(ali.alineacion(str(misPersonas_totalmente_vacunadas_por_centenar), 12))
        cadena += " | "+str(ali.alineacion(str(misPersonas_vacunadas_por_centenar), 12))
        cadena += " | "+str(ali.alineacion(str(miVacunación_total_por_centenar), 12))+" |"
        print(cadena)
        print("| -------------------------------------------------------------------- |")
        
    def visualizarTabla(self):
        self.encabezadoTabla()
        i=0
        miPais = self.filPais
        miVacunacion_diaria = self.filVacunacion_diaria
        miVacunacion_diaria_por_millon = self.filVacunacion_diaria_por_millon
        miVacunacion_diaria_raw = self.filVacunacion_diaria_raw
        miVacunacion_fecha = self.filVacunacion_fecha
        miIso_code = self.filIso_code
        misPersonas_totalmente_vacunadas = self.filPersonas_totalmente_vacunadas
        misPersonas_totalmente_vacunadas_por_centenar = self.filPersonas_totalmente_vacunadas_por_centenar
        misPersonas_vacunadas = self.filPersonas_vacunadas
        misPersonas_vacunadas_por_centenar = self.filPersonas_vacunadas_por_centenar
        miNombre_de_búsqueda = self.filNombre_de_búsqueda
        miWeb_de_búsqueda = self.filWeb_de_búsqueda
        miVacunación_total = self.filVacunación_total
        miVacunación_total_por_centenar = self.filVacunación_total_por_centenar
        miVacunas = self.filVacunas
        
        while(len(self.data)>i):
            valida = []
            valida.append(self.validaPais(i, miPais))
            valida.append(self.validaVacunacion_diaria(i, miVacunacion_diaria))
            valida.append(self.validaVacunacion_diaria_por_millon(i, miVacunacion_diaria_por_millon))
            valida.append(self.validaVacunacion_diaria_raw(i, miVacunacion_diaria_raw))
            valida.append(self.validaVacunacion_fecha(i, miVacunacion_fecha))
            valida.append(self.validaIso_code(i, miIso_code))
            valida.append(self.validaPersonas_totalmente_vacunadas(i, misPersonas_totalmente_vacunadas))
            valida.append(self.validaPersonas_totalmente_vacunadas_por_centenar(i, misPersonas_totalmente_vacunadas_por_centenar))
            valida.append(self.validaPersonas_vacunadas(i, misPersonas_vacunadas))
            valida.append(self.validaPersonas_vacunadas_por_centenar(i, misPersonas_vacunadas_por_centenar))
            valida.append(self.validaNombre_de_búsqueda(i, miNombre_de_búsqueda))
            valida.append(self.validaWeb_de_búsqueda(i, miWeb_de_búsqueda))
            valida.append(self.validaVacunación_total(i, miVacunación_total))
            valida.append(self.validaVacunación_total_por_centenar(i, miVacunación_total_por_centenar))
            valida.append(self.validaVacunas(i, miVacunas))
            
            resultado = 0
            for item in valida:
                if(item == 2):
                    resultado = 2
                    break
                else:
                    resultado = 1
                    
            if(resultado == 1):
                self.tomarRegistro(i)
                self.imprimirRegistro()
                
            i = i+1


    def visualizarRegistro(self, i):
        miPais = self.filPais
        miVacunacion_diaria = self.filVacunacion_diaria
        miVacunacion_diaria_por_millon = self.filVacunacion_diaria_por_millon
        miVacunacion_diaria_raw = self.filVacunacion_diaria_raw
        miVacunacion_fecha = self.filVacunacion_fecha
        miIso_code = self.filIso_code
        misPersonas_totalmente_vacunadas = self.filPersonas_totalmente_vacunadas
        misPersonas_totalmente_vacunadas_por_centenar = self.filPersonas_totalmente_vacunadas_por_centenar
        misPersonas_vacunadas = self.filPersonas_vacunadas
        misPersonas_vacunadas_por_centenar = self.filPersonas_vacunadas_por_centenar
        miNombre_de_búsqueda = self.filNombre_de_búsqueda
        miWeb_de_búsqueda = self.filWeb_de_búsqueda
        miVacunación_total = self.filVacunación_total
        miVacunación_total_por_centenar = self.filVacunación_total_por_centenar
        miVacunas = self.filVacunas

        valida = []
        valida.append(self.validaPais(i, miPais))
        valida.append(self.validaVacunacion_diaria(i, miVacunacion_diaria))
        valida.append(self.validaVacunacion_diaria_por_millon(i, miVacunacion_diaria_por_millon))
        valida.append(self.validaVacunacion_diaria_raw(i, miVacunacion_diaria_raw))
        valida.append(self.validaVacunacion_fecha(i, miVacunacion_fecha))
        valida.append(self.validaIso_code(i, miIso_code))
        valida.append(self.validaPersonas_totalmente_vacunadas(i, misPersonas_totalmente_vacunadas))
        valida.append(self.validaPersonas_totalmente_vacunadas_por_centenar(i, misPersonas_totalmente_vacunadas_por_centenar))
        valida.append(self.validaPersonas_vacunadas(i, misPersonas_vacunadas))
        valida.append(self.validaPersonas_vacunadas_por_centenar(i, misPersonas_vacunadas_por_centenar))
        valida.append(self.validaNombre_de_búsqueda(i, miNombre_de_búsqueda))
        valida.append(self.validaWeb_de_búsqueda(i, miWeb_de_búsqueda))
        valida.append(self.validaVacunación_total(i, miVacunación_total))
        valida.append(self.validaVacunación_total_por_centenar(i, miVacunación_total_por_centenar))
        valida.append(self.validaVacunas(i, miVacunas))
        
        resultado = 0
        for item in valida:
            if(item == 2):
                resultado = 2
                break
            else:
                resultado = 1
                
        if(resultado == 1):
            self.tomarRegistro(i)
            self.verRegistro()
