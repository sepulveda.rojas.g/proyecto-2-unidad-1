import csv
import json

#modelo 
class ConsultaCSV:
    
    def __init__(self, archivo):
        self.archivo = archivo

    def cunsultaListasCampos(self):
        with open(self.archivo, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            return reader.fieldnames

    def consultaRegistros(self, lista):
        with open(self.archivo, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for fila in reader:
                cadena = str()
                for item in lista:
                    cadena = cadena + fila[item] + " "
                print(cadena)

    def creaListaDatos(self, lista):
        data = []
        with open(self.archivo, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for fila in reader:
                cadena = {}
                for item in lista:
                    cadena[item] = fila[item]
                data.append(cadena)
        return data

    def transformaJson(self, data):
        contenido = json.dumps(data, sort_keys=True, indent=4)
        with open("data.json","w") as archivo:
            archivo.write(contenido)


#controlador
class Controlador:
    
    def __init__(self, archivo):
        self.archivo = archivo
        self.consultaCSV = ConsultaCSV(self.archivo)
    
    def consultarCampos(self):
        resultado = self.consultaCSV.cunsultaListasCampos()
        return resultado

    def consultarDatos(self, lista):
        self.consultaCSV.consultaRegistros(lista)
        
    def creaListaDatos(self, lista):
        data = self.consultaCSV.creaListaDatos(lista)
        return data
        
    def transformaJson(self, data):
        self.consultaCSV.transformaJson(data)

archivo = "country_vaccinations.csv"
control = Controlador(archivo)
resultado = control.consultarCampos()
data1 = control.creaListaDatos(resultado)

archivo = "country_vaccinations_by_manufacturer.csv"
control = Controlador(archivo)
resultado = control.consultarCampos()
data2 = control.creaListaDatos(resultado)

data = {}
data["country_vaccinations"] = data1
data["by_manufacturer"] = data2

control.transformaJson(data)

