def alineacion(cadena, campoTamano):
    # toma el tamano de la palabra 
    nroCaracteres = len(str(cadena))
    
    diferencia = campoTamano - nroCaracteres
    
    resultado = ""
    
    if(diferencia < 0):
        # si el contenido es mayor al campo
        #retorna solo parte del contenido para no dañar la tabla
        return cadena[0:campoTamano-3]+"..."
    else:
        # recorre el tamano del campo
        i=0 
        while(campoTamano > i):
            if(i >= diferencia):
                #imprimira los espacios vacios acumulados y la cadena
                resultado += cadena
                break
            else:
                #adiciona una espacio vacio
                #este se acumularará para quede alineado a la derecha
                resultado += " "
            i = i+1
        return resultado

